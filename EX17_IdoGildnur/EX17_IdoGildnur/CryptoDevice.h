#pragma once

#include <cstdio>
#include <iostream>
#include "osrng.h"
#include "modes.h"
#include <string.h>
#include <cstdlib>
#include <md5.h>
#include <rsa.h>

using namespace std;


class CryptoDevice
{

public:
	CryptoDevice();
	~CryptoDevice();
	string encryptAES(string);
	string decryptAES(string);
	string md5Encryption(string msg);
	string rsaEncryption(string publicKey);
	string rsaDecryption();

};
